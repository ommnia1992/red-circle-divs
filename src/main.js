import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'


Vue.config.productionTip = false
Vue.prototype.$axios = axios.create({
  baseURL: process.env.NODE_ENV === 'production'
    ? 'https://red-compass.site'
    : 'https://red-compass.site'
})
Vue.prototype.$months = [
  'января',
  'февраля',
  'марта',
  'апреля',
  'мая',
  'июня',
  'июля',
  'августа',
  'сентября',
  'октября',
  'ноября',
  'декарбря',
]

new Vue({
  render: h => h(App),
}).$mount('#app')


// export function vueApp(el, baseUrl) {
//     Vue.prototype.$axios = axios.create({
//       baseURL: baseUrl || '/'
//     })
//     new Vue({
//       render: h => h(App),
//     }).$mount(el)
//   }

// import { vueApp } from './vue-app.common'
//
// document.addEventListener('DOMContentLoaded', function () {
//   console.log('12');
//   setTimeout(()=>{
//     vueApp(document.getElementById('app'), 'https://red-compass.site/')
//   },3000)
//   console.log(vueApp);
//
// })
