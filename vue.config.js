module.exports = {

  // add red-circle-divs for gitlab pages
  publicPath: process.env.NODE_ENV === 'production'
    ? '/red-circle-divs'
    : '/red-circle-divs',
  lintOnSave: false,
  productionSourceMap: false,
  css: {
    extract: false
  }
}
